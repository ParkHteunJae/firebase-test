﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Firebase;
using Firebase.Unity.Editor;
using Firebase.Database;


public class User
{
    public int id;
    public int score;

    public User(int id, int score)
    {
        this.id = id;
        this.score = score;
    }
}
public class DataManager : MonoBehaviour
{
    private DatabaseReference reference;
    public DatabaseReference GetReference => reference;
    private Queue<Task> Tasks;
    private readonly ushort threadCount = 5;
    private object criticalSection;

    private void DatabaseInit(ref DatabaseReference databaseReference)
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://fir-test-cb927.firebaseio.com/");
        databaseReference = FirebaseDatabase.DefaultInstance.RootReference;
    }
    

    public void AppendUser(int id, int score)
    {
        User user = new User(id, score);
        string json = JsonUtility.ToJson(user);

        GetReference.Child("users").Push().SetRawJsonValueAsync(json);
    }

    public void GetUserInfo()
    {
        GetReference.Child("users").GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted == true)
            {
                DataSnapshot snapshot = task.Result;

                Parallel.ForEach(snapshot.Children, (DataSnapshot data) =>
                 {
                     GetReference.Child("users").OrderByChild($"{data.Key}/score");

                     IDictionary userInfo = (IDictionary)data.Value;
                     Debug.Log($"id : {userInfo["id"]}, score : {userInfo["score"]}");
                 });
            }
        });
    }
    private void DatabaseUpdate()
    {
        int sequence = 0;
        Tasks = new Queue<Task>();
        criticalSection = new object();

        for (int j = 0; j < threadCount; j++)
        {
            Task thread = Task.Run(() => {
                Parallel.For(0, 20, (int i) =>
                {
                    lock (criticalSection)
                    {
                        AppendUser(sequence, sequence);
                        sequence++;
                    }
                });
            });
            Tasks.Enqueue(thread);
        }
        Parallel.ForEach(Tasks, (Task T) =>
        {
            T.Wait();
        });
    }
    // Use this for initialization
    void Start()
    {
        DatabaseInit(ref reference);
        DatabaseUpdate();
        GetUserInfo();
    }
}

